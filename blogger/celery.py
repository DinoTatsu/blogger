# blogger/celery.py

import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'blogger.settings')

app = Celery('blogger')
app.config_from_object('django.conf:settings')
app.conf.timezone = 'Asia/Bishkek'

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

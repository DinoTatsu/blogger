
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from blogger import settings
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Blogger Mitapp API')


urlpatterns = [
    path('', include('core.urls')),
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    # path('api-auth/', include('rest_framework.urls')),
    path('docs/', schema_view),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from django.utils.timezone import now
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from blogger.settings import SELF_HOST
from core.models import User, Post, Blog, UserLikePost, UserHidePost, UserWatchPost


class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = (
            'id',
            'user',
            'blog_id',
            'blog_title',
            'blog_description',
            'image',
            'blog_image_url',
        )

    blog_image_url = serializers.SerializerMethodField()

    def create(self, validated_data):
        blog = Blog.objects.create(**validated_data)
        return blog

    def get_blog_image_url(self, instance):
        if instance.image:
            return f'http://{SELF_HOST}{instance.image.url}'


class PostListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'userid',
            'publish_at',

            'id',
            'blog_id',
            'blog',

            'post_id',
            'post_title',
            'image',
            'content',
            'post_link',

            'post_image_url',
            'updated_at',
            'likes_count',
            'watches_count',
            'like',
        )
        read_only_fields = ('id', 'blog', 'updated')

    blog = BlogSerializer(required=False)
    userid = serializers.CharField(write_only=True, required=True)
    publish_at = serializers.DateTimeField(write_only=True, required=False)
    blog_id = serializers.CharField(write_only=True)
    like = serializers.SerializerMethodField()
    likes_count = serializers.SerializerMethodField()
    watches_count = serializers.SerializerMethodField()
    post_image_url = serializers.SerializerMethodField()
    user = None

    def get_post_image_url(self, instance):
        if instance.image:
            return f'http://{SELF_HOST}{instance.image.url}'

    def get_user(self, userid):
        if not self.user:
            self.user = User.objects.get(userid=userid)
        return self.user

    def validate(self, attrs):
        self.get_user(attrs.pop('userid', None))  # avoid sending it back to model creation
        return super(PostListSerializer, self).validate(attrs)

    def get_like(self, obj):
        try:
            userid = self.context['request'].data.get('userid')
            if not userid:
                userid = self.context['request'].query_params.get('userid')
            if obj.likes.filter(user__userid=userid):
                return True
        except:
            return False
        return False

    def get_likes_count(self, obj):
        return obj.likes.count()

    def get_watches_count(self, obj):
        return obj.watched.count()


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'userid',
            'publish_at',

            'id',
            'blog_id',
            'blog',

            'post_id',
            'post_title',
            'image',
            'content',
            'post_link',

            'post_image_url',
            'updated_at',
            'likes_count',
            'watches_count',
            'like',
        )
        read_only_fields = ('id', 'blog', 'updated')

    blog = BlogSerializer(required=False)
    userid = serializers.CharField(write_only=True, required=True)
    publish_at = serializers.DateTimeField(write_only=True, required=False)
    blog_id = serializers.CharField(write_only=True)
    like = serializers.SerializerMethodField()
    likes_count = serializers.SerializerMethodField()
    watches_count = serializers.SerializerMethodField()
    post_image_url = serializers.SerializerMethodField()
    user = None

    def get_post_image_url(self, instance):
        if instance.image:
            return f'http://{SELF_HOST}{instance.image.url}'

    def get_user(self, userid):
        if not self.user:
            self.user = User.objects.get(userid=userid)
        return self.user

    def validate(self, attrs):
        self.get_user(attrs.pop('userid', None))  # avoid sending it back to model creation
        return super(PostSerializer, self).validate(attrs)

    def create(self, validated_data):
        blog_id = validated_data.pop('blog_id', None)
        blog = Blog.objects.get(blog_id=blog_id)
        post = Post.objects.create(**validated_data, blog=blog)
        return post

    def update(self, instance, validated_data):
        validated_data.update({'updated_at': now()})
        return super(PostSerializer, self).update(instance, validated_data)

    def get_like(self, obj):
        try:
            userid = self.context['request'].data.get('userid')
            if obj.likes.filter(user__userid=userid):
                return True
        except:
            return False
        return False

    def get_likes_count(self, obj):
        return obj.likes.count()

    def get_watches_count(self, obj):
        return obj.watched.count()


class UserLikePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLikePost
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(queryset=UserLikePost.objects.all(), fields=['user', 'post'])
        ]


class UserHidePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserHidePost
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(queryset=UserHidePost.objects.all(), fields=['user', 'post'])
        ]


class UserWatchPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserWatchPost
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(queryset=UserHidePost.objects.all(), fields=['user', 'post'])
        ]


class BlogAndLastPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = (
            'user',
            'blog_id',
            'blog_title',
            'blog_description',
            'image',
            'blog_image_url',
            'last_post',
        )

    last_post = serializers.SerializerMethodField()
    blog_image_url = serializers.SerializerMethodField()

    def get_blog_image_url(self, instance):
        if instance.image:
            return f'http://{SELF_HOST}{instance.image.url}'

    def get_last_post(self, obj):
        last_post = obj.get_last_post()
        if last_post:
            return PostListSerializer(last_post, context=self.context).data
        else:
            return None

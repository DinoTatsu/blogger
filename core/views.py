from django.contrib.auth import login
from django.db.models import Count, F, ExpressionWrapper, DurationField, Q
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.utils import timezone
from django.views.generic.base import View

from rest_framework import generics, status
from rest_framework.exceptions import APIException
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from blogger.settings import blogger_client_id
from core.models import User, Post, UserLikePost, UserHidePost, UserWatchPost, Blog
from core.serializers import PostSerializer, UserLikePostSerializer, UserHidePostSerializer, UserWatchPostSerializer, \
    BlogAndLastPostSerializer, PostListSerializer

CLIENT_ID = blogger_client_id.get('web').get('client_id')  # '<client-id>'
CLIENT_SECRET = blogger_client_id.get('web').get('client_secret')  # '<client-secret>'


# получать user и access_token из параметров в каждом запросе
class GetUserMixin(generics.GenericAPIView):
    user = None

    def get_user(self):
        if not self.request.META.get('HTTP_AUTHORIZATION'):
            raise APIException(detail='no auth parameters', code=status.HTTP_401_UNAUTHORIZED)
        access_token = self.request.META.get('HTTP_AUTHORIZATION')[7:]
        # userid = self.request.data.get('userid')
        user = User.objects.filter(access_token=access_token).first()
        if not user:
            user = User.create_with_google_info(access_token=access_token)
            if not user:
                raise ValueError('Token not valid')
            user.get_self_blogs_from_google()  # получает все блоги пользователя при первой авторизации
        login(self.request, user, backend='django.contrib.auth.backends.ModelBackend')
        return user
    
    def dispatch(self, request, *args, **kwargs):
        try:
            self.user = self.get_user()
        except Exception as error:
            return JsonResponse(status=status.HTTP_401_UNAUTHORIZED, data={'error': str(error)})
        return super(generics.GenericAPIView, self).dispatch(request, *args, **kwargs)


class SetTokenView(View):
    def get(self, request):
        login_failed_url = '/'
        if 'error' in request.GET or 'code' not in request.GET:
            return HttpResponseRedirect('{loginfailed}'.format(loginfailed=login_failed_url))

        access_token = User.get_access_token(self.request.GET.get('code'))
        user = User.create_with_google_info(access_token)
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        return JsonResponse({'success': True, 'access_token': user.access_token})


class PostListCreateView(GetUserMixin,
                         UpdateModelMixin,
                         generics.ListCreateAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        # self.get_user()
        post_list = Post.objects.exclude(hides__user__userid=self.get_user().userid)
        return post_list

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PostUpdateView(UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def update(self, request, *args, **kwargs):
        from rest_framework.response import Response
        instance = self.get_object()
        serializer = PostSerializer(instance=instance, data=request.data, partial=True)
        serializer.is_valid()
        serializer.save()
        if request.data.get('post_title') or request.data.get('content'):
            instance.google_update()
        return Response(serializer.data)


class PostPublishView(UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.google_publish()
        return super(PostPublishView, self).update(request, *args, **kwargs)


# POST post/{id}/like
class PostLikeCreateAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserLikePostSerializer
    queryset = UserLikePost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        like, created = UserLikePost.objects.get_or_create(post=post, user=self.user)
        return Response(UserLikePostSerializer(like).data, status=status.HTTP_201_CREATED)


# POST post/{id}/unlike
class PostLikeDeleteAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserLikePostSerializer
    queryset = UserLikePost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        UserLikePost.objects.filter(post=post, user=self.user).delete()
        return Response(status=200)


# POST post/{id}/hide
class PostHideCreateAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserHidePostSerializer
    queryset = UserHidePost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        hidden, created = UserHidePost.objects.get_or_create(post=post, user=self.user)
        return Response(UserHidePostSerializer(hidden).data, status=status.HTTP_201_CREATED)


# POST post/{id}/unhide
class PostHideDeleteAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserHidePostSerializer
    queryset = UserHidePost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        UserHidePost.objects.filter(post=post, user=self.user).delete()
        return Response(status=200)


# POST post/{id}/watch
class PostWatchCreateAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserWatchPostSerializer
    queryset = UserWatchPost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        watched, created = UserWatchPost.objects.get_or_create(post=post, user=self.user)
        return Response(UserWatchPostSerializer(watched).data, status=status.HTTP_201_CREATED)


# POST post/{id}/unwatch
class PostWatchDeleteAPIView(GetUserMixin, CreateAPIView):
    serializer_class = UserWatchPostSerializer
    queryset = UserWatchPost.objects.all()

    def post(self, request, *args, **kwargs):
        post = Post.objects.get(post_id=kwargs.get('post_id'))
        UserWatchPost.objects.filter(post=post, user=self.user).delete()
        return Response(status=200)


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

'''
создание поста может быть отложенным, проверять перед публикацией
'''


# GET post/popular по формуле
class PopularPostListView(GetUserMixin, generics.ListAPIView):
    serializer_class = PostListSerializer
    model = Post
    pagination_class = StandardResultsSetPagination
    #  Нужно посчитать очки для каждого поста по формуле: кол-во поставленных лайков/время ”существования”
    #  Взять за “эталон” пост с наибольшим количеством очков
    #  Вычислить рейтинг поста по формуле: (очки поста/очки “эталона”)*100 и после округлить число до целых
    #  После динамически обновлять рейтинг при появлении новых “лайков” и постов (т.е. вернуться к п. 1)

    def get_queryset(self):
        now = timezone.now()
        queryset = Post.published\
            .exclude(hides__user__userid=self.user.userid) \
            .annotate(like_count=Count('likes')) \
            .annotate(life_time=ExpressionWrapper(now - F('updated_at'), output_field=DurationField()))\
            .order_by('likes')
        new_qs = []
        for item in queryset:
            if item not in new_qs:
                setattr(item, 'score', item.like_count/item.life_time.seconds)
                new_qs.append(item)
        sorted_by_score = sorted(new_qs, key=lambda x: getattr(x, 'score'), reverse=True)
        return sorted_by_score


# GET post/recent  по дате
class RecentPostListView(GetUserMixin, generics.ListAPIView):
    serializer_class = PostListSerializer
    model = Post
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        queryset = Post.published.exclude(hides__user__userid=self.user.userid).distinct().order_by('-updated_at')
        return queryset


# GET post/own  свои посты
class OwnPostListView(GetUserMixin, generics.ListAPIView):
    serializer_class = PostListSerializer
    model = Post
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        queryset = Post.published.filter(blog__user__userid=self.user.userid).distinct().order_by('-updated_at')
        return queryset


# GET blog/blog_id/post  список постов блога
class BlogPostListView(GetUserMixin, generics.ListAPIView):
    serializer_class = PostListSerializer
    model = Post
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        blog_id = self.kwargs.get('blog_id')
        queryset = Post.published.filter(blog__blog_id=blog_id).distinct().order_by('-updated_at')
        return queryset


# GET user/userid/post  список постов созданных пользователем
class UserPostListView(GetUserMixin, generics.ListAPIView):
    serializer_class = PostListSerializer
    model = Post
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        userid = self.kwargs.get('userid')
        queryset = Post.published.filter(blog__user__userid=userid).distinct().order_by('-updated_at')
        return queryset


# POST   post  # create
class PostCreateView(GetUserMixin, generics.CreateAPIView):
    serializer_class = PostSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# GET post/{id}
# DELETE post/{id}
# PATCH  post/{id}
class PostRetrieveUpdateDestroyAPIView(GetUserMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_object(self):
        post_id = self.kwargs.get('post_id')
        try:
            return Post.objects.get(post_id=post_id)
        except Post.DoesNotExist:
            raise Http404

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if self.user.userid != instance.blog.user.userid:
            return JsonResponse(status=status.HTTP_401_UNAUTHORIZED, data={'error': 'Not an owner'})
        return super(PostRetrieveUpdateDestroyAPIView, self).update(request, *args, **kwargs)


# GET blog/{id}
# PATCH  blog/{id}
# GET get/blog/blog_id
'''
1 раз запрос в blogger, получаю инфу о блоге
отдаю инфу из БД

+ последний пост блога
'''
# PATCH blog/blog_id
class BlogRetrieveUpdateAPIView(GetUserMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = BlogAndLastPostSerializer
    queryset = Blog.objects.all()
    http_method_names = ['get', 'patch', 'head']

    def get_object(self):
        blog_id = self.kwargs.get('blog_id')
        blog = Blog.objects.filter(blog_id=blog_id).first()
        if blog and blog.blog_title:
            return blog
        else:
            return Blog.create(self.user.access_token, blog_id)
    
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance or not getattr(instance, 'user'):
            return Response(status=404, data={'error': 'Объект не найден'})
        return super(BlogRetrieveUpdateAPIView, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance or not getattr(instance, 'user'):
            return Response(status=404, data={'error': 'Объект не найден'})
        if self.user.userid != instance.user.userid:
            return Response(status=403, data={'error': 'Not an owner'})
        return super(BlogRetrieveUpdateAPIView, self).update(request, *args, **kwargs)

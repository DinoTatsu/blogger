import requests

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

from blogger.settings import blogger_client_id, api_key
from core.models import User

test_blog_id = '2842041165505008771'
# 2842041165505008771 - https://www.blogger.com/blogger.g?blogID=2842041165505008771#allposts
# 3213900 - developer blog


def create_post(blogId, access_token, isDraft):
    params = {
        "isDraft": isDraft,
        "title": "test 5",
        "content": "542 test content",
    }
    url = f'https://www.googleapis.com/blogger/v3/blogs/{blogId}/posts'
    headers = {
        'Authorization': f"Bearer {access_token}",
        'Content-Type': 'application/json'
    }
    response = requests.post(url, json=params, headers=headers)
    return response


def get_blog(blogId, access_token):
    url = f'https://www.googleapis.com/blogger/v3/blogs/{blogId}'
    params = {
        'blogId': blogId,
        'maxPosts': 10,
    }
    headers = {
        'Authorization': f"Bearer {access_token}"
    }
    response = requests.get(url, params, headers=headers)
    return response


def get_user(access_token):
    url = f'https://www.googleapis.com/blogger/v3/users/self'
    params = {
        # 'blogId': blogId,
        # 'maxPosts': 10,
    }
    headers = {
        'Authorization': f"Bearer {access_token}"
    }
    response = requests.get(url, params, headers=headers)
    return response


def make_request():
    token_request_uri = "https://accounts.google.com/o/oauth2/auth"
    response_type = "code"
    client_id = blogger_client_id.get('web').get('client_id')  # your_client_id
    redirect_uri = "http://localhost:8011/google/oauth2callback"
    scope = "https://www.googleapis.com/auth/blogger"
    url = f"{token_request_uri}?response_type={response_type}&client_id={client_id}&redirect_uri={redirect_uri}&scope={scope}"
    print('url', url)


def get_google_token(code):
    access_token_uri = 'https://accounts.google.com/o/oauth2/token'
    redirect_uri = "http://localhost:8011/google/oauth2callback"
    params = {
        'code': code,
        'redirect_uri': redirect_uri,
        'client_id': blogger_client_id.get('web').get('client_id'),
        'client_secret': blogger_client_id.get('web').get('client_secret'),
        'grant_type': 'authorization_code'
    }
    headers = {
        'content-type': 'application/x-www-form-urlencoded'
    }
    r = requests.post(access_token_uri, params, headers=headers)

    # User.create(r.json().get('access_token'))
    return r.json()


def rqw(request):
    login_failed_url = '/'
    if 'error' in request.GET or 'code' not in request.GET:
        return HttpResponseRedirect('{loginfailed}'.format(loginfailed = login_failed_url))

    token_data = get_google_token(request.GET['code'])
    # print(token_data)

    r2 = get_blog(test_blog_id, token_data['access_token'])
    r3 = create_post(test_blog_id, token_data['access_token'], True)
    r4 = get_user(token_data['access_token'])
    print('r3.text', r3.text)
    print('r4.text', r4.text)
    return JsonResponse(r3.json())

import requests
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from blogger.celery import app
from blogger.settings import blogger_client_id


class CustomUserManager(BaseUserManager):
    def create_user(self, email=None, userid=None, password=None, is_staff=False, is_superuser=False):
        model_data = {'is_staff': is_staff, 'is_superuser': is_superuser}

        if email:
            model_data['email'] = self.normalize_email(email)
        if not userid:
            raise ValueError('User must have a userid')
        model_data.update({
            'userid': userid,
        })

        user = self.model(**model_data)
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, userid=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, userid, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ['-last_login',]

    userid = models.CharField('Google user id', unique=True, max_length=127, null=True, blank=True)
    access_token = models.CharField('Google access token', max_length=255, null=True, blank=True)
    name = models.CharField('name', max_length=127, null=True, blank=True)
    password = models.CharField(_('password'), max_length=128, null=True, blank=True)

    is_staff = models.BooleanField('Админ', default=False)
    is_superuser = models.BooleanField('Суперпользователь', default=False)
    is_active = models.BooleanField('Активен', default=True)

    last_login = models.DateTimeField('Дата последней авторизации', null=True, blank=True)

    USERNAME_FIELD = 'userid'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return f'{self.name}'

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def get_self_blogs_from_google(self):
        url = f'https://www.googleapis.com/blogger/v3/users/{self.userid}/blogs'
        headers = {
            'Authorization': f"Bearer {self.access_token}"
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        for item in response_json.get('items'):
            self.blog_set.get_or_create(
                blog_id=item.get('id'),
                defaults={
                    'name': item.get('name'),
                    'blog_description': item.get('description'),
                }
            )

    # не нужно
    def get_self_posts_from_google(self, blogId):
        url = f'https://www.googleapis.com/blogger/v3/blogs/{blogId}/posts'
        headers = {
            'Authorization': f"Bearer {self.access_token}"
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        for item in response_json.get('items'):
            self.post_set.get_or_create(
                post_id=item.get('id'),
                defaults={
                    'url': item.get('url'),
                    'post_title': item.get('title'),
                    'content': item.get('content'),
                }
            )

    @staticmethod
    def create_with_google_info(access_token):
        url = f'https://www.googleapis.com/blogger/v3/users/self'
        headers = {
            'Authorization': f"Bearer {access_token}"
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()

        google_user_id = response_json.get('id')
        google_name = response_json.get('displayName')
        if google_user_id:
            user, created = User.objects.get_or_create(
                userid=google_user_id,
                defaults={
                    'name': google_name,
                }
            )
            user.last_login = now()
            user.access_token = access_token
            user.save()
            user.get_self_blogs_from_google()
            return user
        return None

    @staticmethod
    def get_access_token(code):
        access_token_uri = 'https://accounts.google.com/o/oauth2/token'
        redirect_uri = "http://localhost:8011/google/oauth2callback"
        params = {
            'code': code,
            'redirect_uri': redirect_uri,
            'client_id': blogger_client_id.get('web').get('client_id'),
            'client_secret': blogger_client_id.get('web').get('client_secret'),
            'grant_type': 'authorization_code'
        }
        headers = {
            'content-type': 'application/x-www-form-urlencoded'
        }
        r = requests.post(access_token_uri, params, headers=headers)
        access_token = r.json().get('access_token')
        return access_token


class Blog(models.Model):
    class Meta:
        verbose_name_plural = 'Блоги'
        verbose_name = 'блог'
        ordering = ['id']

    user = models.ForeignKey('User', models.CASCADE, null=True, blank=True)
    blog_id = models.CharField('Google blog id', max_length=127, unique=True)
    blog_title = models.CharField('name', max_length=255, null=True, blank=True)
    blog_description = models.TextField('description', null=True, blank=True)
    image = models.ImageField('image', upload_to='blog', null=True, blank=True)

    def __str__(self):
        return f'{self.blog_title}'

    def get_last_post(self):
        return self.post_set.exclude(publish_at__gt=now()).order_by('-updated_at').first()

    @staticmethod
    def get_from_google(blog_id, access_token):
        url = f'https://www.googleapis.com/blogger/v3/blogs/{blog_id}'
        headers = {
            'Authorization': f"Bearer {access_token}"
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json

    @staticmethod
    def create(access_token, blogId):
        response_json = Blog.get_from_google(blogId, access_token)
        user = User.objects.filter(access_token=access_token).first()
        if not user:
            user = User.create_with_google_info(access_token=access_token)
            user.get_self_blogs_from_google()
        if response_json.get('id'):
            blog, created = Blog.objects.get_or_create(
                user=user,
                blog_id=response_json.get('id'),
                blog_title=response_json.get('name'),
                blog_description=response_json.get('description'),
            )
            return blog
        return None

    def create_post(self, title, image, content, is_draft):
        params = {
            "isDraft": is_draft,
            "title": title,
            "content": content,
        }
        url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog_id}/posts'
        headers = {
            'Authorization': f"Bearer {self.user.access_token}",
            'Content-Type': 'application/json'
        }
        response = requests.post(url, json=params, headers=headers)
        item = response.json()
        self.post_set.create(
            post_id=item.get('id'),
            url=item.get('url'),
            post_title=item.get('title'),
            content=item.get('content'),
            image=image,
        )


class PublishedPostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().exclude(publish_at__gt=now())


class Post(models.Model):
    class Meta:
        verbose_name_plural = 'Post'
        verbose_name = 'post'
        ordering = ['-updated_at']
    # paginate_by = 10
    # watched отправляется post запросом
    # get latest post and blog with blog_id

    blog = models.ForeignKey('Blog', models.CASCADE, null=True, blank=True)
    post_id = models.CharField('Google post id', max_length=255, unique=True)

    post_title = models.CharField('custom title', max_length=255, null=True, blank=True)
    image = models.ImageField('Image', upload_to='post', null=True, blank=True)
    content = models.TextField('Google content', null=True, blank=True)
    post_link = models.URLField('Google link', null=True, blank=True)

    updated_at = models.DateTimeField(auto_now=True)
    publish_at = models.DateTimeField('Отложенная публикация', null=True, blank=True)
    objects = models.Manager()
    published = PublishedPostManager()

    def __str__(self):
        return f'{self.post_title}'

    @staticmethod
    @app.task
    def create(post_id, hours=0, minutes=0, seconds=0):
        if hours or minutes or seconds:
            seconds = hours*3600 + minutes*60 + seconds
            User.objects.get_or_create.apply_async(countdown=seconds)
        else:
            User.objects.get_or_create()

    def get_likes(self):
        return self.likes.count()

    def get_watches(self):
        return self.watched.count()

    # def publish(self):
    #     url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog_id}/posts/{self.postid}/publish'
    #     headers = {
    #         'Authorization': f"Bearer {self.blog.user.access_token}",
    #         'Content-Type': 'application/json'
    #     }
    #     response = requests.post(url, headers=headers)
    #     if response.status_code == 200:
    #         self.published = True
    #         self.save()
    #
    # def google_draft(self):
    #     url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog.blog_id}/posts/{self.postid}/revert'
    #     headers = {
    #         'Authorization': f"Bearer {self.blog.user.access_token}"
    #     }
    #     response = requests.post(url, headers=headers)
    #     item = response.json()
    #
    # def google_create(self):
    #     params = {
    #         "isDraft": False,
    #         "title": self.title,
    #         "content": self.content,
    #     }
    #     url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog.blog_id}/posts'
    #     headers = {
    #         'Authorization': f"Bearer {self.blog.user.access_token}",
    #         'Content-Type': 'application/json'
    #     }
    #     response = requests.post(url, json=params, headers=headers)
    #     item = response.json()
    #     self.postid = item.get('id')
    #     self.updated = now()
    #     self.link = item.get('url')
    #     self.save()
    #     if False:
    #         self.google_draft()
    #     return item
    #
    # def google_update(self):
    #     url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog.blog_id}/posts/{self.postid}'
    #     params = {
    #         "title": self.title,
    #         "content": self.content,
    #     }
    #     headers = {
    #         'Authorization': f"Bearer {self.blog.user.access_token}",
    #         'Content-Type': 'application/json'
    #     }
    #     response = requests.patch(url, json=params, headers=headers)
    #     item = response.json()
    #     self.updated = now()
    #     self.save()
    #
    # def google_publish(self):
    #     url = f'https://www.googleapis.com/blogger/v3/blogs/{self.blog.blog_id}/posts/{self.postid}/publish'
    #     headers = {
    #         'Authorization': f"Bearer {self.blog.user.access_token}",
    #         'Content-Type': 'application/json'
    #     }
    #     response = requests.post(url, headers=headers)
    #     item = response.json()
    #     self.updated = now()
    #     self.published = now()
    #     self.save()


class UserLikePost(models.Model):
    class Meta:
        verbose_name_plural = 'Like'
        verbose_name = 'Like'
        ordering = ['id']

    user = models.ForeignKey('User', models.CASCADE, related_name='likes')
    post = models.ForeignKey('Post', models.CASCADE, related_name='likes')
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user} - {self.post}'


class UserHidePost(models.Model):
    class Meta:
        verbose_name_plural = 'Посты, скрытые пользователями'
        verbose_name = 'пост'
        ordering = ['id']

    user = models.ForeignKey('User', models.CASCADE, related_name='hides')
    post = models.ForeignKey('Post', models.CASCADE, related_name='hides')
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user} - {self.post}'


class UserWatchPost(models.Model):
    class Meta:
        verbose_name_plural = 'Посты, просмотренные пользователем'
        verbose_name = 'пост'
        ordering = ['id']

    user = models.ForeignKey('User', models.CASCADE, related_name='watched')
    post = models.ForeignKey('Post', models.CASCADE, related_name='watched')
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user} - {self.post}'

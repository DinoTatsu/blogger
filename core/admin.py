from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from core.models import User, Post, Blog, UserLikePost


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_display = ('userid', 'last_login')
    # list_filter = ('is_staff', 'is_superuser')
    search_fields = ('userid', 'name')
    ordering = ('last_login',)
    fieldsets = (
        (None, {'fields': ('userid', 'access_token', 'name',)}),
        (_('Permissions'), {'fields': ('is_superuser', 'is_staff', 'is_active')}),
        (_('Important dates'), {'fields': ('last_login', )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('userid', 'password1', 'password2'),
        }),
    )


class BlogAdmin(admin.ModelAdmin):
    list_display = [
        'blog_id',
        'blog_title',
    ]
    list_display_links = [
        'blog_id',
        'blog_title',
    ]


class PostAdmin(admin.ModelAdmin):
    list_display = [
        'post_id',
        'post_title',
        'blog',
    ]
    list_display_links = [
        'post_id',
        'post_title',
        'blog',
    ]


class UserLikePostAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'user',
        'post',
    ]
    list_display_links = [
        'id',
        'user',
        'post',
    ]
    search_fields = ['user__userid', 'post__post_id']


admin.site.register(Post, PostAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(UserLikePost, UserLikePostAdmin)

from django.urls import path, include

from core.views import SetTokenView, PostListCreateView, PostUpdateView, PostPublishView, PostLikeCreateAPIView, \
    PostLikeDeleteAPIView, PostCreateView, OwnPostListView, BlogPostListView, UserPostListView, RecentPostListView, \
    PostRetrieveUpdateDestroyAPIView, PostHideCreateAPIView, PostHideDeleteAPIView, PostWatchCreateAPIView, \
    BlogRetrieveUpdateAPIView, PopularPostListView

urlpatterns = [
    path('google/oauth2callback', SetTokenView.as_view(), name='set_token'),
    # path('post/', PostListCreateView.as_view(), name='post_list_create'),
    # path('post/<int:pk>/', PostUpdateView.as_view(), name='post_update'),
    # path('post/<int:pk>/publish', PostPublishView.as_view(), name='post_publish'),
    # path('google/oauth2callback', rqw, name='asdf'),
    path('post/', PostCreateView.as_view(), name='post_create'),  # checked
    path('post/<int:post_id>', PostRetrieveUpdateDestroyAPIView.as_view(), name='post_get_update_del'),  # checked
    path('post/own', OwnPostListView.as_view(), name='self_post_list'),  # checked
    path('post/recent', RecentPostListView.as_view(), name='recent_post_list'),  # checked
    path('post/popular', PopularPostListView.as_view(), name='popular_post_list'),  # checked
    path('blog/<str:blog_id>/post', BlogPostListView.as_view(), name='blog_post_list'),  # checked
    path('user/<str:userid>/post', UserPostListView.as_view(), name='user_post_list'),  # checked

    path('post/<int:post_id>/like', PostLikeCreateAPIView.as_view(), name='post_like'),  # checked
    path('post/<int:post_id>/unlike', PostLikeDeleteAPIView.as_view(), name='post_unlike'),  # checked
    path('post/<int:post_id>/hide', PostHideCreateAPIView.as_view(), name='post_hide'),  # checked
    path('post/<int:post_id>/show', PostHideDeleteAPIView.as_view(), name='post_show'),  # checked
    path('post/<int:post_id>/watched', PostWatchCreateAPIView.as_view(), name='post_watch'),  # checked

    path('blog/<str:blog_id>', BlogRetrieveUpdateAPIView.as_view(), name='blog_get_update'),  # checked
]

# получать userid из параметров в каждом запросе
'''
создание поста может быть отложенным, проверять перед публикацией
'''

# GET post/own  свои посты
# GET post/popular по формуле
# GET post/recent  по дате

# DELETE post/{id}
# PATCH  post/{id}
# POST   post  # create

# POST post/{id}/hide
# POST post/{id}/unhide
# POST post/{id}/like
# POST post/{id}/unlike
# POST post/{id}/watch
# POST post/{id}/unwatch

# GET get/blog/blog_id
'''
1 раз запрос в blogger, получаю инфу о блоге
отдаю инфу из БД

+ последний пост блога
'''
# PATCH blog/blog_id

# GET blog/blog_id/post  список постов блога
# GET blog/userid/post  список постов созданных пользователем
